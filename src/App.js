
import "./App.css";
import Header from "./component/Header";
import Footer from "./component/Footer";

function App() {
  return (
    <div className="container-fluid pe-0 ps-0">
      <Header />

      <div className="container-fluid m-5">
        <h1 style={{color:"#FE0000"}} >Pandit Care will Available for All Types of Puja and Sanskar</h1>
        <p className="fs-4 text-secondary">
          We Provides Best Pandit ji in Delhi, Uttar Pradesh, Bengaluru and many
          more cities With the highly qualified and experienced Pandit, Priest,
          Purohit, Pujari, Guruji, Acharya Brahmin,Shastri for requirements of
          Puja, different solutions and services are now available online all
          kind of panditji.
        </p>
        <div className="text-center">
        <button type="button " className="btn text-white" style={{backgroundColor:"#FE0000"}}>
          Book Now
        </button>
        </div>
      </div>

      <div className="d-flex flex-sm-row flex-row justify-content-center align-items-center gap-5">
        <img src="./homeceremony.jpg" className="  mw-100 px-5"  alt=""/>
        <div>
        <h2 style={{color:"#FE0000"}}>We Cover All Types of Rituals</h2>
        <p className="fs-4 text-secondary">
          We cover significant rituals such as Hawan, Shubh Vivah
          -Wedding/marriage ceremony, Satyanarayan Katha, Griha Pravesh,
          Namkaran Sanskar or naming ceremony, Nava Graha Shanti, Laxmi Puja,
          Ganesh Puja, Ramayan, Bhagwat Katha, Vastu Shanti, Sunderkand Puja,
          and many more.
        </p>
        </div>
      </div>

      <div className="">
        <h1 className="text-uppercase  fw-bold mb-4 m-5 text-center" style={{color:"#FE0000"}}>
          Puja Services Provided By Us
        </h1>
        <div className="d-flex flex-wrap justify-content-evenly fs-4 text-secondary"  >
          <ul >
            <li>Ganesh Puja</li>
            <li>Vrat Katha</li>
            <li>Bhagvat Katha Puja</li>
            <li>Kal Sarp Shanti</li>
            <li>Garud Puraan</li>
            <li>Tulsi Vivah</li>
          </ul>
          <ul>
            <li>Naam Karan Sanskar</li>
            <li>Annaparashan Puja</li>
            <li>Janeyu sanskar</li>
            <li>Vastu Shanti</li>
            <li>Vivah Puja</li>
            <li>Laxmi Puja</li>
          </ul>

          <ul>
            <li>Bhoomi Pujan</li>
            <li>Opening A New Shop</li>
            <li>Mundan Puja</li>
            <li>Kumbh Vivha</li>
            <li>Pitra Shanti</li>
            <li>Grabh Puja</li>
          </ul>

          <ul>
            <li>Durga Path</li>
            <li>Durga Sapstati Puja</li>
            <li>Grih Parvesh</li>
            <li>Ganesh Pratisthapan</li>
            <li>Ganesh Puja</li>
            <li>Havan</li>
          </ul>
        </div>
      </div>

      <Footer />
    </div>
  );
}

export default App;
