import React, { Component } from "react";
import { GiHamburgerMenu } from "react-icons/gi";
import { Link } from "react-router-dom";

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div>
        <nav
          className="navbar  navbar-expand-lg py-3 py-sm-0 mx-0 my-0  sticky-top pos-f-t"
          style={{ backgroundColor: "#FE0000" }}
        >
          <div className="container-fluid">
            <img style={{ width: "120px" }} src="./logo-pc.jpg" alt=""></img>
            <button
              className="navbar-toggler border border-black "
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <GiHamburgerMenu color="white" />
            </button>

            <div
              className="collapse navbar-collapse"
              id="navbarSupportedContent"
            >
              <ul className="navbar-nav ms-auto ">
                <li className="nav-item  py-lg-4 mx-2">
                  <Link
                    className="nav-link text-white fs-4 "
                    id="RouterNavLink"
                    to="/home"
                  >
                    Home
                  </Link>
                </li>

                <li className="nav-item py-lg-4 mx-2">
                  <Link
                    className="nav-link text-white fs-4 "
                    id="RouterNavLink1"
                    to="/pujaservice"
                  >
                    Puja service
                  </Link>
                </li>

                <li className="nav-item py-lg-4 mx-2">
                  <a className="nav-link text-white fs-4 " href="#">
                    Service Area
                  </a>
                </li>
                <li className="nav-item py-lg-4 mx-2">
                  <a className="nav-link text-white fs-4" href="#">
                    About
                  </a>
                </li>
                <div className="d-flex justify-content-center align-items-center  ">
                  <Link
                    className="nav-link text-white fs-4 "
                    id="RouterNavLink2"
                    to="/login"
                  >
                    Login
                  </Link>
                </div>
              </ul>
            </div>
          </div>
        </nav>
      </div>
    );
  }
}

export default Header;
