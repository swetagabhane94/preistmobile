import React, { Component } from "react";
import Header from "./Header";
import Footer from "./Footer";

class Pujaservice extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div
        id="puja"
        style={{
          backgroundImage: "url(./design1.jpg)",
          backgroundSize: "cover",
          backgroundRepeat: "no-repeat",
          backgroundAttachment: "fixed",
        }}
      >
        <Header />
        <div className="container-fluid">
          <h4 className="text-center  py-5 " style={{ color: "#FE0000" }}>
            
            Book Pandit Ji for All Types of Puja
          </h4>
          <p className="fw-bold fs-4 text-justify text-center pb-5">
            We provide all types of puja services at your home or nearby
            locations like Bengaluru, Delhi and many more city.You can check in
            service city detail page.
          </p>
        </div>
        <section className="d-flex justify-content-center align-items-center h-100 min-vh-100">
          <div className="container">
            <div className="card-deck row gap-2">
              <div className="col-lg-3 col-md-4">
                <div className="card h-100 d-flex p-4 flex-column">
                  <img className="card-img-top" src="./ganesh.jpg" alt="" />
                  <div className="card-body">
                    <h4 className="card-title" style={{ color: "#FE0000" }}>
                      Ganesh Puja
                    </h4>
                    <p>
                      Find experienced and reliable pandits for Ganesh puja
                      ceremonies in your place. Ensure a blessed and auspicious
                      celebration by hiring an experienced North Indian Pandit
                      through our convenient booking service.
                    </p>
                    <button
                      type="button"
                      className="btn text-light mt-auto"
                      style={{ backgroundColor: "#FE0000" }}
                    >
                      Book Now
                    </button>
                  </div>
                </div>
              </div>

              <div className="col-lg-3 col-md-4">
                <div className="card h-100 d-flex p-4 flex-column">
                  <img
                    className="card-img-top"
                    src="./satyanarayan.jpg"
                    alt=""
                  />
                  <div className="card-body">
                    <h4 className="card-title " style={{ color: "#FE0000" }}>
                      Satyanarayana Puja
                    </h4>
                    <p>
                      Looking to book a pandit for Satyanarayana Puja in home?
                      Find experienced pandits for Satyanarayana Puja ceremonies
                      in your place and ensure a smooth and auspicious ritual.
                      Perform the traditional Satyanarayana Puja with expert
                      guidance.
                    </p>
                    <button
                      type="button"
                      className="btn text-light mt-auto "
                      style={{ backgroundColor: "#FE0000" }}
                    >
                      Book Now
                    </button>
                  </div>
                </div>
              </div>
              <div className="col-lg-3 col-md-4">
                <div className="card h-100 d-flex p-4 flex-column">
                  <img className="card-img-top" src="./shivji.png" alt="" />
                  <div className="card-body">
                    <h4 className="card-title" style={{ color: "#FE0000" }}>
                      Rudrabhishek Puja
                    </h4>
                    <p>
                      Looking to book a pandit for Rudrabhishek Puja in Delhi?
                      Find experienced pandits for Puja ceremonies in your place
                      and ensure a smooth and auspicious ritual. Perform the
                      traditional Satyanarayana Puja with expert guidance.
                    </p>
                    <button
                      type="button"
                      className="btn text-light mt-auto"
                      style={{ backgroundColor: "#FE0000" }}
                    >
                      Book Now
                    </button>
                  </div>
                </div>
              </div>

              <div className="col-lg-3 col-md-4">
                <div className="card h-100 d-flex p-4 flex-column">
                  <img className="card-img-top" src="./wedding.jpg" alt="" />
                  <div className="card-body ">
                    <h4 className="card-title" style={{ color: "#FE0000" }}>
                      Marriage ceremony
                    </h4>
                    <p>
                      Marriage ceremony is one of the most important events of
                      one's life. This is indicative of transitional phase from
                      Brahmacharayashram to Grihasthashramam.Looking to book a
                      pandit for wedding rituals? Book Priest for your wedding.
                    </p>
                    <button
                      type="button"
                      className="btn text-light mt-auto"
                      style={{ backgroundColor: "#FE0000" }}
                    >
                      Book Now
                    </button>
                  </div>
                </div>
              </div>

              <div className="col-lg-3 col-md-4">
                <div className="card h-100 d-flex p-4 flex-column">
                  <img className="card-img-top" src="./birth.jpg" alt="" />
                  <div className="card-body">
                    <h4 className="card-title" style={{ color: "#FE0000" }}>
                      Namkaran Puja
                    </h4>
                    <p>
                     
                      Namkaran is the naming ceremony of the child, it is very
                      important as it’s the first ceremony of a child’s life.
                      Graha Shanti and havan are performed for the well-being of
                      the child to get all the blessings for a healthy and happy
                      life.
                    </p>
                    <button
                      type="button"
                      className="btn  text-light mt-auto"
                      style={{ backgroundColor: "#FE0000" }}
                    >
                      Book Now
                    </button>
                  </div>
                </div>
              </div>

              <div className="col-lg-3 col-md-4">
                <div className="card h-100 d-flex p-4 flex-column">
                  <img
                    className="card-img-top"
                    src="./homeceremony.jpg"
                    alt=""
                  />
                  <div className="card-body">
                    <h4 className="card-title" style={{ color: "#FE0000" }}>
                      Griha Pravesh Puja
                    </h4>
                    <p>
                      Time is everything and matters a lot especially when it
                      comes to Griha Pravesh. As per the Hindu Calendar, Griha
                      Pravesh is a ceremony that ushers good fortune and
                      positivity to your new home and life if performed on
                      specific days and time.
                    </p>
                    <button
                      type="button"
                      className="btn text-light mt-auto"
                      style={{ backgroundColor: "#FE0000" }}
                    >
                      Book Now
                    </button>
                  </div>
                </div>
              </div>

              <div className="col-lg-3 col-md-4">
                <div className="card h-100 d-flex p-4 flex-column">
                  <img className="card-img-top" src="./grahadosh.jpg" alt="" />
                  <div className="card-body">
                    <h4 className="card-title" style={{ color: "#FE0000" }}>
                      Graha Shanti Puja
                    </h4>
                    <p>
                      
                      Graha Shanti Puja is performed to eliminate the ill
                      effects, dosh of the bad planets. This puja also increases
                      the flow of positive energy in the person and family.
                      Performing Graha Shanti also bestows the dwellers of the
                      home with peace and prosperity.
                    </p>
                    <button
                      type="button"
                      className="  mt-auto text-light btn"
                      style={{ backgroundColor: "#FE0000" }}
                    >
                      Book Now
                    </button>
                  </div>
                </div>
              </div>

              <div className="col-lg-3 col-md-4">
                <div className="card h-100 d-flex p-4 flex-column">
                  <img
                    className="card-img-top"
                    src="./navgrah-puja.jpg"
                    alt=""
                  />
                  <div className="card-body">
                    <h4 className="card-title" style={{ color: "#FE0000" }}>
                      Navagraha Puja
                    </h4>
                    <p>
                      The Sun, the Moon, Mars, Mercury, Jupiter, Venus, Saturn
                      and the two shadow planets Rahu and Ketu constitute the
                      Navagrahas. The Nine "grahas" or planets in our horoscope
                      control our karma, our desires and their outcomes{" "}
                    </p>
                    <button
                      type="button"
                      className="btn text-light mt-auto"
                      style={{ backgroundColor: "#FE0000" }}
                    >
                      Book Now
                    </button>
                  </div>
                </div>
              </div>

              <div className="col-lg-3 col-md-4">
                <div className="card h-100 d-flex p-4 flex-column">
                  <img className="card-img-top"  src="./laxmi.jpg" alt="" />
                  <div className="card-body">
                    <h4 className="card-title" style={{ color: "#FE0000" }}>
                      Diwali Puja
                    </h4>
                    <p>
                      Find experienced and reliable pandits for Diwali &
                      Mahalaxmi puja ceremonies in Mumbai. Ensure a blessed and
                      auspicious celebration by hiring an experienced North
                      Indian Pandit through our convenient booking service.
                    </p>
                    <button
                      type="button"
                      className="btn text-light mt-auto"
                      style={{ backgroundColor: "#FE0000" }}
                    >
                      Book Now
                    </button>
                  </div>
                </div>
              </div>

              <div className="col-lg-3 col-md-4">
                <div className="card h-100 d-flex p-4 flex-column">
                  <img className="card-img-top" src="./bhumi.jpg" alt="" />
                  <div className="card-body">
                    <h4 className="card-title" style={{ color: "#FE0000" }}>
                      Bhumi Pujan
                    </h4>
                    <p>
                      
                      Ensure a successful and spiritually fulfilling start to
                      your new project or construction with our trusted Pandits
                      with Trusted Priests. Discover Experienced Pandits,
                      Hassle-Free Booking Process, and Timely Services for Your
                      Bhoomi Puja in Bangalore
                    </p>
                    <button
                      type="button"
                      className="btn text-light mt-auto"
                      style={{ backgroundColor: "#FE0000" }}
                    >
                      Book Now
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <Footer />
      </div>
    );
  }
}

export default Pujaservice;
