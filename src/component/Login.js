import React, { Component } from "react";
import Header from "./Header";
import Signup from "./Signup";


class Login extends Component {
  constructor() {
    super();
    this.state = {};
  }
  render() {
    return (
      <div>
        <Header />
        <div className="d-flex  flex-wrap justify-content-around align-items-center">
          <div>
            <h1 style={{ color: "#FE0000" }}>Pandit Care</h1>
            <p className="fs-5">
              Pandit care provides best services at your city, connect with us.
            </p>
          </div>

          <div className="card  my-5 mw-100">
            <div className="card-body ">
              <form>

                <div className="mb-3">
                  <input
                    type="email"
                    className="form-control"
                    id="exampleInputEmail1"
                    aria-describedby="emailHelp"
                    placeholder="Email address or phone number"
                  />
                </div>

                <div className="mb-3">
                  <input
                    type="password"
                    className="form-control"
                    id="exampleInputPassword1"
                    placeholder="Password"
                  />
                </div>

                <button type="submit" className="btn btn-primary w-100">
                  Log In
                </button>
                <p className="text-center">Forgotten password?</p>
              </form>
              <hr />
              
              <div className="text-center">
                <button
                  type="submit"
                  className="btn text-white"
                  style={{ backgroundColor: "#FE0000" }}
                  data-bs-toggle="modal"
                  data-bs-target="#staticBackdrop"
                >
                  Create new account
                </button>

                <div
                  className="modal fade"
                  id="staticBackdrop"
                  data-bs-backdrop="static"
                  data-bs-keyboard="false"
                  tabIndex="-1"
                  aria-labelledby="staticBackdropLabel"
                  aria-hidden="true"
                >
                  <div className=" modal-dialog modal-dialog-centered">
                    <div className="modal-content">
                      <div className="modal-header">
                        <h5 className="modal-title" id="staticBackdropLabel">
                          Sign Up
                        </h5>

                        <button
                          type="button"
                          className="btn-close"
                          data-bs-dismiss="modal"
                          aria-label="Close"
                        ></button>
                      </div>
                      <div className="modal-body">
                        <Signup />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Login;
