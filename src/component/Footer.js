import React, { Component } from "react";
import { FaFacebookF, FaTwitter, FaGoogle, FaInstagram } from "react-icons/fa6";

class Footer extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div
        className="text-center  text-lg-start text-muted"
        style={{ backgroundColor: "#FE0000" }}
      >
        <section className="d-flex justify-content-center justify-content-lg-between p-4 border-bottom">
          <div className="me-5 d-none text-white d-lg-block">
            <span>Get connected with us on social networks:</span>
          </div>

          <div>
            <FaFacebookF color="white" />
            <FaTwitter color="white" />
            <FaGoogle color="white" />
            <FaInstagram color="white" />
          </div>
        </section>

        <section className="">
          <div className=" container text-center text-md-start mt-5">
            <div className="mt-3 row">
              <div className="mx-auto mb-4 col col-3 col-lg-4">
                <h6 className="text-white fw-bold mb-4">Pandit Care</h6>
                <p className="text-white">
                  We Performs All types of Rituals Like General Puja, Special
                  Puja, Sanskar Puja, Festival Puja, Shanti Puja
                </p>
              </div>

              <div className="mx-auto text-white mb-4 col-md-3 col-lg-2">
                <h6 className="text-uppercase fw-bold mb-4">Useful links</h6>
                <p>Pricing</p>
                <p>Settings</p>
                <p>Orders</p>
                <p>Help</p>
              </div>

              <div className="mx-auto mb-md-0 mb-4 col-md-4 col-lg-3 text-white">
                <h6 className="text-uppercase  fw-bold mb-4">Contact</h6>
                <p>Raipur, 492001, IN</p>
                <p>info@example.com</p>
                <p>9999-999-999</p>
                <p>1000-100-101</p>
              </div>
            </div>
          </div>
        </section>

        <div
          className="text-center text-white p-4"
          style={{ backgroundColor: "rgba(0, 0, 0, 0.05)" }}
        >
          © 2021 Copyright:
          <a className="text-reset fw-bold" href="https://mdbootstrap.com/">
            MDBootstrap.com
          </a>
        </div>
      </div>
    );
  }
}

export default Footer;
